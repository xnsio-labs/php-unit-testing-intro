<?php

class Stack
{
    private $arr;

    public function __construct()
    {
        $this->arr = [];
    }

    public function push($element)
    {
        $this->arr[] = $element;
    }

    public function pop()
    {
        return array_pop($this->arr);
    }

    public function peek()
    {
        return end(array_values($this->arr));
    }

    public function isEmpty()
    {
        return empty($this->arr);
    }

    public function size()
    {
        return count($this->arr);
    }
}