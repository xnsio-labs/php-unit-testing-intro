<?php
use PHPUnit\Framework\TestCase;

final class EmailTest extends TestCase
{
    public function testCanBeCreatedFromValidEmailAddress(): void
    {
        $emailId = 'user@example.com';
        $email = Email::fromString($emailId);
        $this->assertInstanceOf(Email::class, $email);
        $this->assertEquals($emailId, $email->__toString());
    }

    public function testCannotBeCreatedFromInvalidEmailAddress(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Email::fromString('invalid');
    }

    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals('user@example.com', Email::fromString('user@example.com'));
    }
}